package org.rockyang.tio.core.server;

import org.tio.core.intf.Packet;

/**
 * 消息包实体
 *
 * @author yangjian
 */
public class PingPacket extends BasePacket {
	private static final long serialVersionUID = -172060606924066412L;
	public static final int HEADER_LENGTH = 4;//消息头的长度
	public static final String CHARSET = "utf-8";
	private byte[] body;

	/**
	 * @return the body
	 */
	@Override
	public byte[] getBody() {
		return body;
	}

	/**
	 * @param body the body to set
	 */
	@Override
	public void setBody(byte[] body) {
		this.body = body;
	}
}
